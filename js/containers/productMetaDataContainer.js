import { connect } from 'react-redux'
import { selectPublisherData,saveProductMetaData,selectBoxData} from '../actions/productMetaData'
import ProductMetaData from '../components/ProductMetaData'

const getSelectedValues = (dataArray) => {
  if (dataArray.length > 0) {
    return dataArray[dataArray.length-1];
  }
  return [];
}


const mapStateToProps = (state) => {
  var data = getSelectedValues(state.productMetaData);
     var productName = '';
  //  if (data.PublisherData) {
     if(state.autoComplete.length > 0){
        productName = state.autoComplete[state.autoComplete.length-1].text;
     }
      return {
            disciplinesData:data.DisciplinesData,         
            publisherData: data.PublisherData,
            suggestions: data.suggestions,
            errMsg: data.errMsg,
          "initialValues": {
            productName:productName,
            author:data.Author,
            ISBN:data.ISBN,
            disciplinesData : (data.disciplinesData)?encodeURIComponent(JSON.stringify(data.disciplinesData[0])):"",
            publisherData : (data.publisherData)?encodeURIComponent(JSON.stringify(data.publisherData[0])):"",
            tags: data.tags
        }
      }
 //   }
    /*else {
      console.log(state.form.ProductMetadataComponent, 'props')
      return state.form.ProductMetadataComponent;
    }*/
}

const handleDelete = (obj) => {
        var tags = this.state.tags;
        tags.splice(i, 1);
        this.setState({ tags: tags });
    }
const handleAddition = (obj) => {
        var tags = this.state.tags;
        tags.push({
            id: tags.length + 1,
            name: tag.name
        });
        this.setState({ tags: tags });
    }



const mapDispatchToProps = (dispatch) => {
  return {
   handleDelete: handleDelete,
   handleAddition: handleAddition,
   componentWillMount () {
        dispatch(selectBoxData());
   },
      linkData(value,dispatch){
      console.log(value);
       dispatch(selectPublisherData(value.productName));
       console.log("Fetched");
   },
   onSave (value, dispatch) {
      console.log(value);
      dispatch(saveProductMetaData(value));
      console.log("success");
   }
  }
}


const ProductMetaDataContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(ProductMetaData)

export default ProductMetaDataContainer