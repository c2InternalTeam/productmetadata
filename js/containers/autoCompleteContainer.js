import { connect } from 'react-redux'
import { populateAutoComplete } from '../actions'
//import { populateAutoComplete} from '../actions/productMetaData'
import AutoComplete from '../components/autoComplete'

const getSelectedValues = (dataArray) => {debugger;
  if (dataArray.length > 0) {
    return dataArray[dataArray.length-1];
  }

  return [];
}

const mapStateToProps = (state) => {
  return {
    data: getSelectedValues(state.autoComplete)
  }
}


const mapDispatchToProps = (dispatch) => {
  return {
    onSuggestionsUpdateRequested: ({value}) => {
     dispatch(populateAutoComplete(value));
      console.log(value);
    }
  }
}

const AutoCompleteContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(AutoComplete)

export default AutoCompleteContainer
