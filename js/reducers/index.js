import { combineReducers } from 'redux'
import productMetaData from './productMetaDataReducers'
import autoComplete from './autoCompleteReducer'
import {reducer as formReducer} from 'redux-form';

export default combineReducers({
  autoComplete,
  productMetaData,
  form: formReducer
})

