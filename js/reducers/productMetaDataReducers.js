import * as CONST from '../constants/productMetaDataConstants' 
import Immutable from 'immutable' 

var initilizeData = [{
      ProductName:"",
      Author:"",
      ISBN:"",
      Publisher:"",
      Disciplines:"",
      PublisherData:[],
      DisciplinesData:[],
      tags:[],
      suggestions: [],
      autoComplete:[],
      errMsg: ''
}]

const productMetaDataReducers= (state = initilizeData, action) => {
try{
  switch (action.type) {
    case CONST.PRODUCT_META_DATA:
      return[
        Immutable.fromJS(state[0]).merge(Immutable.Map(action.PMD_Data)).toJS() 
      ]
     break;

     case CONST.PRODUCT_META_DATA_ERROR:
     let retRevData = Object.assign({}, state[0], {
            errMsg: action.PMD_Data_Err
          });
    console.log("returned in reducer --->",retRevData);
    return [retRevData];
     break;

    case CONST.SAVE_PRODUCT_META_DATA:
     return[
      Immutable.fromJS(state[0]).merge(Immutable.Map(action.values)).toJS() 
     ]
      break;

     case CONST.SAVE_PRODUCT_META_DATA_ERROR:
     let retMetaSaveData = Object.assign({}, state[0], {
            errMsg: action.SPMD_Data_Err
          });
    console.log("returned in reducer --->",retMetaSaveData);
    return [retMetaSaveData];
     break;
      
    case CONST.META_DATA:
     return[
       Immutable.fromJS(state[0]).merge(Immutable.Map(action.PMD_Data)).toJS() 
     ]
      break;

      case CONST.META_DATA_ERROR:
     let retMetaData = Object.assign({}, state[0], {
            errMsg: action.PMD_Data_Err
          });
    console.log("returned in reducer --->",retMetaData);
    return [retMetaData];
     break;
    default:
      return state
  }
}catch(error){
    console.log(error);
  }
}

export default productMetaDataReducers
