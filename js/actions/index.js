import autoCompleteApi from '../api/autoCompleteApi'
import productMetaDataApi from '../api/productMetaDataApi'
import store from '../store';
import * as CONST from '../constants/productMetaDataConstants' 


export function  populateAutoComplete (text){
    return dispatch => {
      productMetaDataApi.autoComplete_Data(text).then(function(data){ 
        dispatch({
          type: CONST.AUTO_COMPLETE,
          data : JSON.parse(data.text),
          text
        })
      })
  }
}