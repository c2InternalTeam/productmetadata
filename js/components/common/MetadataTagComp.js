"use strict";
import React from 'react';
import Heading from './Heading';
import TagElem from'./TagElem';

class MetadataTag extends React.Component{
constructor(props) {
    super(props);
    this.displayName = 'MetadataTag';
}
state=  {
}
render() {
        return (
        <div>
             <Heading headingType="h3" value={this.props.heading} />
             <TagElem suggestions={this.props.suggestions} tags={this.props.tags}/>
        </div>
        )
    }
};
module.exports = MetadataTag;