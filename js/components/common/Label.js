/**
 * Copyright
 * All rights reserved.
 *
 * This source code is licensed under the BSD-style license found in the
 * LICENSE file in the root directory of this source tree. An additional grant
 * of patent rights can be found in the PATENTS file in the same directory.
 */

/**
 * This component operates as a "Controller-View".  It listens for changes in
 * the TodoStore and passes the new data to its children.
 */
 'use strict';
import React from 'react';


class Label extends React.Component{
	constructor(props) {
	    super(props);
	    this.displayName = 'Label';
  	}
    static propTypes = {
        for: React.PropTypes.string,
        text : React.PropTypes.string.isRequired,    
    }
    static defaultProps ={
          for:'',
          text: ''
    }
    render() {
        let forTxt = this.props.for;
        let text = this.props.text;

        //below is the pearson label
       // var divAsLabel = "<div class='pe-label pe-label--bold' >{text}</div>";
        return (
            <label htmlFor={forTxt}> {text} </label>
        );
    }

};

module.exports = Label;
