/**
 * Copyright
 * All rights reserved.
 *
 * This source code is licensed under the BSD-style license found in the
 * LICENSE file in the root directory of this source tree. An additional grant
 * of patent rights can be found in the PATENTS file in the same directory.
 */

/**
 * This component operates as a "Controller-View".  It listens for changes in
 * the TodoStore and passes the new data to its children.
 */
"use strict";
import React from 'react';


class Heading extends React.Component{
  constructor(props) {
    super(props);
    this.displayName = 'Heading';
  }
	static propTypes = {
		value: React.PropTypes.string.isRequired,
		headingType : React.PropTypes.string.isRequired,	
	}
	static defaultProps = {
		  value: 'Default Heading',
		  headingType:'h3',
		
  }
	state= {
            value: this.props.value,
            headingType: this.props.headingType,
  }
  render() {
        var style = {
          heading:{
           backgroundColor: '#ECEAEA'
          }
        };
        
		//need to handle heading type also
        return (

            <h3 style={style.heading}>{this.props.value}</h3>
        )
  }

};

module.exports = Heading;
