/**
* Copyright
* All rights reserved.
*
* This source code is licensed under the BSD-style license found in the
* LICENSE file in the root directory of this source tree. An additional grant
* of patent rights can be found in the PATENTS file in the same directory.
*/
 
/**
* This component operates as a "Controller-View".  It listens for changes in
* the TodoStore and passes the new data to its children.
*/
"use strict";
import React, {Component, PropTypes} from 'react';
import Label from './common/Label';
import TextBox from './common/TextBox';
import Heading from './common/Heading';
import SelectBox from './common/SelectBox';
import AutoCompleteContainer from '../containers/autoCompleteContainer';
import TagElem from'./common/TagElem';
import {reduxForm} from 'redux-form';
import {injectIntl, intlShape} from 'react-intl';
import {messages} from './productMetadataDefaultMessages';
 
class ProductMetadataComponent extends React.Component{
  static PropTypes = {
        intl: intlShape.isRequired
    }

  constructor(props) {
    super(props);
    
    this.state = {
      publisherData: [],
      disciplinesData: []
    }
    this.componentWillMount = props.componentWillMount;
    this.onSave = props.onSave;
    this.linkData = props.linkData;
     this.onSuggestionsUpdateRequested = props.onSuggestionsUpdateRequested;
  }

  componentWillReceiveProps (nextProps) {
    if (nextProps.disciplinesData) {
      this.state.disciplinesData = nextProps.disciplinesData;
    }
    if (nextProps.publisherData) {
      this.state.publisherData = nextProps.publisherData;
    }
  }
 
  onBlur (e) {
    return true;
  }

  onChange (e) {
    return true;
  }
 
render(){
  const {formatMessage} = this.props.intl;
 const publisherData = this.state.publisherData || [];
  const disciplinesData = this.state.disciplinesData || [];
 
const {
        fields: {productName,author, ISBN, publisher, disciplines, tags}, handleSubmit
      } = this.props
      if (tags.value === '') {
      tags.value = [];
    }
return(
        <form >
          <div className="pe-updatemetadata">
          <section>    
              <div>     
                <h2>{formatMessage(messages.Product_Metadata_Heading)}</h2>
              </div>
              <div className="pe-metadata-mvm">
              <div className="pe-input pe-input--horizontal">
                <Label for="productName" text={formatMessage(messages.Product_Metadata_Name_Label)}/>
                <AutoCompleteContainer id="productName" value ={productName}/>
                <button name="subject" onClick={handleSubmit(this.linkData)} type="submit" value="HTML">{formatMessage(messages.Product_Metadata_Name_Link)}</button>
              </div >
              <div className="pe-input pe-input--horizontal">
                <Label for="author" text={formatMessage(messages.Product_Metadata_Name_Author)}/>
                <TextBox id="author"  value ={author} />
              </div>
              <div className="pe-input pe-input--horizontal">
                <Label for="ISBN" text={formatMessage(messages.Product_Metadata_ISBN)}/>
                <TextBox id="ISBN"  value ={ISBN} />
              </div>
              <div className="pe-input pe-input--horizontal">
                <Label for ="publisher" text={formatMessage(messages.Product_Metadata_Publisher)}/>
                <SelectBox id="publisher" value={publisher} onChange={this.onChange} onBlur= {this.onBlur} options={publisherData}>
                </SelectBox>
              </div>      
              <div className="pe-input pe-input--horizontal">
                <Label for ="disciplines" text={formatMessage(messages.Product_Metadata_Disciplines)}/>
                <SelectBox id="disciplines" value={disciplines} onChange= {this.onChange} onBlur= {this.onBlur} options={disciplinesData}>
                </SelectBox>
              </div> 
              <div className="pe-input pe-input--horizontal">
                <h2>{formatMessage(messages.Product_Level_Tags)}</h2>
                </div>
              <div className="pe-input pe-input--horizontal">
                <button name="subject" onClick={handleSubmit(this.onSave)} type="submit" value="HTML">{formatMessage(messages.Product_Level_Save)}</button>
              </div>
              <div className="pe-input pe-input--horizontal">
                <TagElem suggestions={this.props.suggestions} tags={tags.value}/>
              </div>
              </div>
          </section>
          </div>
        </form>
    )}
};
 
ProductMetadataComponent = reduxForm({
  form: 'ProductMetadataComponent',                          
  fields: ['author', 'ISBN','publisher', 'disciplines', 'productName', 'tags']
})(ProductMetadataComponent);
 
//module.exports = ProductMetadataComponent;
export default injectIntl(ProductMetadataComponent);