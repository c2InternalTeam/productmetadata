import {defineMessages} from 'react-intl';

export const messages = defineMessages({
  Product_Metadata_Heading: {
        id: 'Product_Metadata_Heading',
        defaultMessage: 'Product Metadata',
    },
	Product_Metadata_Name_Label :{
		id: 'Product_Metadata_Name_Label',
	    defaultMessage: 'Product Name(Book or Course Title)',
	},
	Product_Metadata_Name_Link :{
		id: 'Product_Metadata_Name_Link',
	    defaultMessage: 'Link',
	},
    Product_Metadata_Name_Author :{
  	id: 'Product_Metadata_Name_Author',
     defaultMessage: 'Author',
  },
   Product_Metadata_ISBN :{
  	id: 'Product_Metadata_ISBN',
     defaultMessage: 'ISBN (optional)',
  },
  Product_Metadata_Publisher :{
  	id: 'Product_Metadata_Publisher',
     defaultMessage: 'Publisher',
  },    
  Product_Metadata_Disciplines :{
  	id: 'Product_Metadata_Disciplines',
     defaultMessage: 'Disciplines',
  },    
  Product_Level_Tags :{
  	id: 'Product_Level_Tags',
     defaultMessage: 'Product Level Tags',
  },
  Product_Level_Save :{
  	id: 'Product_Level_Save',
     defaultMessage: 'Save',
  }		 
})