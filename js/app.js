import React from 'react'
import { render } from 'react-dom'
import { Provider } from 'react-redux'
import { createStore } from 'redux'
import todoApp from './reducers'
import App from './components/App'
import store from './store'
import {addLocaleData, IntlProvider} from 'react-intl';
import frLocaleData from 'react-intl/locale-data/fr';
addLocaleData(frLocaleData);
const translations = {
	'fr' : {
			 "Review_Asset_MetaData": "Métadonnées du produit",			 		 
		  }
};
//const locale =  document.documentElement.getAttribute('lang');
const locale = 'en'

render(
  <IntlProvider locale={locale} messages={translations[locale]}>
  <Provider store={store}>
    <App />
  </Provider></IntlProvider>,
  document.getElementById('root')
)
