var baseUrl = "http://localhost:3000/";

var service = {
	assetsData: baseUrl + 'assetsData',
	assetsDataSelectBox: baseUrl + 'assetsDataSelectBox',
	saveAssetsDataPost : baseUrl + 'saveAssetsDataPost',
	autoCompleteDataSugg : baseUrl + 'autoCompleteDataSugg'
}

module.exports = service;