import productMetaDataReducers from '../../js/reducers/productMetaDataReducers'
import * as actions from '../../js/actions/productMetadata'
import expect from 'expect'

describe('Product metadata reducer', () => {
  let initilizeValues = {
      ProductName:'',
      Author:'',
      ISBN:'',
      Publisher:'',
      Disciplines:'',
      PublisherData:[],
     DisciplinesData:[],
     tags:[]
   };

    before('before creating ', function() {
        
       this.ProductName_Data={"data":

        [{
   "name": "C",
   "year": 1972
},{
   "name": "C#",
   "year": 2000
},{
   "name": "C++",
   "year": 1983
},{
   "name": "Clojure",
   "year": 2007
},{
   "name": "Elm",
   "year": 2012
},{
   "name": "Go",
   "year": 2009
},{
   "name": "Haskell",
   "year": 1990
},{
   "name": "Java",
   "year": 1995
},{
   "name": "Javascript",
   "year": 1995
},{
   "name": "Perl",
   "year": 1987
},{
   "name": "PHP",
   "year": 1995
},{
   "name": "Python",
   "year": 1991
},{
   "name": "Ruby",
   "year": 1995
},{
   "name": "Scala",
   "year": 2003
}]
 };

       this.PLT_Data={
             "tags": [
             { "id": 1,"name": "fractions"},
             {"id": 2,"name": "whole numbers"}
             ],
            "suggestions": [
            {"id": 3,"name": "exponential modeling"},
            {"id": 4,"name": "mathematics"}
            ] 
          };

    this.ProductMetaData_Data={
  "Author": "csss sdfsdfasdf",
  "ISBN": "dsds",
  "PublisherData": [{
    "id": 1,
    "label": "Guitar",
    "category": "music"
  }, {
    "id": 2,
    "label": "Cycling",
    "category": "sports"
  }, {
    "id": 3,
    "label": "Hiking",
    "category": "outdoors"
  }],

  "DisciplinesData": [{
    "id": 1,
    "label": "Guitar",
    "category": "music"
  }, {
    "id": 2,
    "label": "Cycling",
    "category": "sports"
  }, {
    "id": 3,
    "label": "Hiking",
    "category": "outdoors"
  }]


  };

});

    it('should handle initial state', () => {
   
  let ret = productMetaDataReducers(undefined,{});
    expect(ret).toEqual({ProductName:'',
      Author:'',
      ISBN:'',
      Publisher:'',
      Disciplines:'',
      PublisherData:[],
     DisciplinesData:[],
     tags:[]});
  });

    it('should handle PLT change', () => {

    let nextState = productMetaDataReducers({}, {
        type:'PRODUCT_META_DATA',
        PMD_Data:{
            "tags": [
             {"id": 1,"name": "fractions"},
             {"id": 2,"name": "whole numbers"}
             ],
            "suggestions": [
            {"id": 3,"name": "exponential modeling"},
            {"id": 4,"name": "mathematics"}
            ] 
          }

    });

    console.log("nextState is ---->",nextState);

    expect(nextState).toEqual([{
            "tags": [
             {"id": 1,"name": "fractions"},
             {"id": 2,"name": "whole numbers"}
             ],
            "suggestions": [
            {"id": 3,"name": "exponential modeling"},
            {"id": 4,"name": "mathematics"}
            ] 
          }]);

    });


 it('should handle PLT change', () => {

    let nextState = productMetaDataReducers({}, {
        type:'SAVE_PRODUCT_META_DATA',
        values:{
            "tags": [
             {"id": 1,"name": "fractions"},
             {"id": 2,"name": "whole numbers"}
             ],
            "suggestions": [
            {"id": 3,"name": "exponential modeling"},
            {"id": 4,"name": "mathematics"}
            ] 
          }

    });

    console.log("nextState is ---->",nextState);

    expect(nextState).toEqual([{
            "tags": [
             {"id": 1,"name": "fractions"},
             {"id": 2,"name": "whole numbers"}
             ],
            "suggestions": [
            {"id": 3,"name": "exponential modeling"},
            {"id": 4,"name": "mathematics"}
            ] 
          }]);

    });
    

});