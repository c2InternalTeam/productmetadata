
import * as actions from '../../js/actions/productMetadata'
import expect from 'expect' //used any testing library
import productMetaDataConstants from '../../js/constants/productMetaDataConstants';
import productMetaDataApi from '../../js/api/productMetaDataApi';
import store from '../../js/store';
import Promise from 'bluebird';
  


  //actions tests for selectPublisherData 
describe('async actions', () => {
  var dispacthSpy;
  var apiSpy;

  before(function () {
     dispacthSpy = sinon.spy(store,"dispatch");
     apiSpy = sinon.spy(productMetaDataApi,"fetch_PMD_Data");
  });

 it('should return a function', () => {
    expect(actions.selectPublisherData()).toBeA('function');
  })

 it('api should be called when fetching Product data', () => {
 
    actions.selectPublisherData()(store.dispatch);
    expect(apiSpy.called).toEqual(true);
 })

  after(function () { 
    dispacthSpy.restore();
    apiSpy.restore();
  });
});


describe('stub api and returning success should work', () => {
  var dispacthSpy;
  let data;
  before(function () {
    data = {"productdata":"abcdefg"};
    data = JSON.stringify(data);
     sinon.stub(productMetaDataApi, "fetch_PMD_Data").returns(Promise.resolve({"text":data}));
     dispacthSpy = sinon.spy(store,"dispatch");
  });
  it('dispatch a product data action', () => {
    actions.selectPublisherData()(store.dispatch);
    setTimeout(function(){
      let dispatchArg = dispacthSpy.args[0][0];
      expect(dispacthSpy.called).toEqual(true);
      expect(dispatchArg.type).toEqual(productMetaDataConstants.PRODUCT_META_DATA);
      expect(dispatchArg.PMD_Data.productdata).toEqual("abcdefg");
      
    },1000)
  });
  after(function () { 
     dispacthSpy.restore();
     productMetaDataApi.fetch_PMD_Data.restore();
  });
});


describe('stub api and returning error should work', () => {
  var dispacthSpy1;
  let data;
  before(function () {
    data = {"productdata":"abcdefg"};
    data = JSON.stringify(data);
    
     sinon.stub(productMetaDataApi, "fetch_PMD_Data").returns(Promise.reject({"error":"error"}));
     dispacthSpy1 = sinon.spy(store,"dispatch");
  });
  it('dispatch a Product Data action', () => {
    actions.selectPublisherData()(store.dispatch);
   
    setTimeout(function(){
      expect(dispacthSpy1.called).toEqual(false); 
    },1000)
  });
  after(function () { 
    dispacthSpy1.restore();
     productMetaDataApi.fetch_PMD_Data.restore();
  });
});

describe('async actions', () => {
  var dispacthSpy;
  var server;

  before(function () {
     dispacthSpy = sinon.spy(store,"dispatch");
     server = sinon.fakeServer.create();
     server.respondWith("GET", "http://localhost:3000/assetsData", [
    200, 
    {"Content-Type": "application/json"}, 
    JSON.stringify([{"uuid":"d123654"}])
    ]);

     //sinon.stub(marklogicMetadataAPI, "get_MVM_Data").yieldsTo("success", {"data":"MVMData"});
  });


  it('fetch Product data should work fine when mocking service', () => {
    actions.selectPublisherData()(store.dispatch);
    server.respond();
    console.log("server respond called");
    setTimeout(function(){
      expect(dispacthSpy.called).toEqual(true);
    },100)
  });

  after(function () { 
    dispacthSpy.restore();
    server.restore(); 
  });
});


// actions tests for selectBoxData 

describe('async actions', () => {
  var dispacthSpy;
  var apiSpy;

  before(function () {
     dispacthSpy = sinon.spy(store,"dispatch");
     apiSpy = sinon.spy(productMetaDataApi,"get_PMD_Data");
  });

 it('should return a function', () => {
    expect(actions.selectBoxData()).toBeA('function');
  })

 it('api should be called when fetching Product data', () => {
    actions.selectBoxData()(store.dispatch);
    expect(apiSpy.called).toEqual(true);
 })

  after(function () { 
    dispacthSpy.restore();
    apiSpy.restore();
  });
});


describe('stub api and returning success should work', () => {
  var dispacthSpy;
  let data;
  before(function () {
    data = {"productdata":"abcdefg"};
    data = JSON.stringify(data);
     sinon.stub(productMetaDataApi, "get_PMD_Data").returns(Promise.resolve({"text":data}));
     dispacthSpy = sinon.spy(store,"dispatch");
  });
  it('dispatch a product data action', () => {
    actions.selectBoxData()(store.dispatch);
    setTimeout(function(){
      let dispatchArg = dispacthSpy.args[0][0];
      expect(dispacthSpy.called).toEqual(true);
      expect(dispatchArg.type).toEqual(productMetaDataConstants.META_DATA);
      expect(dispatchArg.PMD_Data.productdata).toEqual("abcdefg");
      
    },1000)
  });
  after(function () { 
     dispacthSpy.restore();
     productMetaDataApi.get_PMD_Data.restore();
  });
});


describe('stub api and returning error should work', () => {
  var dispacthSpy1;
  let data;
  before(function () {
    data = {"productdata":"abcdefg"};
    data = JSON.stringify(data);
  
     sinon.stub(productMetaDataApi, "get_PMD_Data").returns(Promise.reject({"error":"error"}));
     dispacthSpy1 = sinon.spy(store,"dispatch");
  });
  it('dispatch a Product Data action', () => {
    actions.selectBoxData()(store.dispatch);
   
    setTimeout(function(){
      expect(dispacthSpy1.called).toEqual(false); 
    },1000)
  });
  after(function () { 
    dispacthSpy1.restore();
     productMetaDataApi.get_PMD_Data.restore();
  });
});

describe('async actions', () => {
  var dispacthSpy;
  var server;

  before(function () {
     dispacthSpy = sinon.spy(store,"dispatch");
     server = sinon.fakeServer.create();
     server.respondWith("GET", "http://localhost:3000/assetsDataSelectBox", [
    200, 
    {"Content-Type": "application/json"}, 
    JSON.stringify([{"uuid":"d123654"}])
    ]);

     //sinon.stub(marklogicMetadataAPI, "get_MVM_Data").yieldsTo("success", {"data":"MVMData"});
  });


  it('fetch Product data should work fine when mocking service', () => {
    actions.selectBoxData()(store.dispatch);
    server.respond();
    console.log("server respond called");
    setTimeout(function(){
      expect(dispacthSpy.called).toEqual(true);
    },100)
  });

  after(function () { 
    dispacthSpy.restore();
    server.restore(); 
  });
});

// actions tests for saveProductMetaData

describe('async actions', () => {
  var dispacthSpy;
  var apiSpy;

  before(function () {
     dispacthSpy = sinon.spy(store,"dispatch");
     apiSpy = sinon.spy(productMetaDataApi,"save_PMD_Data");
  });

 it('should return a function', () => {
    expect(actions.saveProductMetaData()).toBeA('function');
  })

 it('api should be called when fetching Product data----', () => {
  debugger;
    actions.saveProductMetaData()(store.dispatch);
    debugger;
    expect(apiSpy.called).toEqual(true);
 })

  after(function () { 
    dispacthSpy.restore();
    apiSpy.restore();
  });
});


describe('stub api and returning success should work', () => {
  var dispacthSpy;
  let data;
  before(function () {
    data = {"productdata":"abcdefg"};
    data = JSON.stringify(data);
     sinon.stub(productMetaDataApi, "save_PMD_Data").returns(Promise.resolve({"text":data}));
     dispacthSpy = sinon.spy(store,"dispatch");
  });
  it('dispatch a product data action', () => {
    actions.saveProductMetaData()(store.dispatch);
    setTimeout(function(){
      let dispatchArg = dispacthSpy.args[0][0];
      expect(dispacthSpy.called).toEqual(true);
      expect(dispatchArg.type).toEqual(productMetaDataConstants.SAVE_PRODUCT_META_DATA);
      expect(dispatchArg.SPMD_Data.productdata).toEqual("abcdefg");
      
    },1000)
  });
  after(function () { 
     dispacthSpy.restore();
     productMetaDataApi.save_PMD_Data.restore();
  });
});


describe('stub api and returning error should work', () => {
  var dispacthSpy1;
  let data;
  before(function () {
    data = {"productdata":"abcdefg"};
    data = JSON.stringify(data);
  
     sinon.stub(productMetaDataApi, "save_PMD_Data").returns(Promise.reject({"error":"error"}));
     dispacthSpy1 = sinon.spy(store,"dispatch");
  });
  it('dispatch a Product Data action', () => {
    actions.saveProductMetaData()(store.dispatch);
  
    setTimeout(function(){
      expect(dispacthSpy1.called).toEqual(false); 
    },1000)
  });
  after(function () { 
    dispacthSpy1.restore();
     productMetaDataApi.save_PMD_Data.restore();
  });
});

describe('async actions', () => {
  var dispacthSpy;
  var server;

  before(function () {
     dispacthSpy = sinon.spy(store,"dispatch");
     server = sinon.fakeServer.create();
     server.respondWith("GET", "http://localhost:3000/saveAssetsDataPost", [
    200, 
    {"Content-Type": "application/json"}, 
    JSON.stringify([{"uuid":"d123654"}])
    ]);

     //sinon.stub(marklogicMetadataAPI, "get_MVM_Data").yieldsTo("success", {"data":"MVMData"});
  });


  it('fetch Product data should work fine when mocking service', () => {
    debugger;
    actions.saveProductMetaData()(store.dispatch);
    server.respond();
    console.log("server respond called");
    setTimeout(function(){
      expect(dispacthSpy.called).toEqual(true);
    },100)
  });

  after(function () { 
    dispacthSpy.restore();
    server.restore(); 
  });
});

