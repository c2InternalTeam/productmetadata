import expect from 'expect' 
import productMetaDataApi from '../../js/api/productMetaDataApi';
import Promise from 'bluebird';

describe('async api', () => {
  var server;
/*
  before(function () {
     server = sinon.fakeServer.create();
     server.respondWith("GET", "http://localhost:3000/assetsData", [
    200, 
    {"Content-Type": "application/json"}, 
    JSON.stringify([{
      "ProductName":'riyaz' 
}])
    ]);
  });

  it('fetch PMD should work fine when mocking service', () => {
    console.log("server respond called");
    debugger;
    let successdata = productMetaDataApi.fetch_PMD_Data('riyaz');
    server.respond();
    successdata.then(function(data){ 
      debugger;
     console.log("data after",data);
      expect(data.text).toEqual(JSON.stringify([{
      "ProductName":'riyaz'
     
}]));
    }, function(error){
      debugger;
      console.log("error");
    });
  });

  after(function () { 
    server.restore(); 
  });

*/
  before(function () {
     server = sinon.fakeServer.create();
     server.respondWith("GET", "http://localhost:3000/assetsDataSelectBox", [
    200, 
    {"Content-Type": "application/json"}, 
    JSON.stringify([{"uuid":"d123654"}])
    ]);
  });

  it('get PMD should work fine when mocking service', () => {
    console.log("server respond called");
    let successdata = productMetaDataApi.get_PMD_Data();
    server.respond();
    successdata.then(function(data){ 
      debugger;
      console.log("data",data);
      expect(data.text).toEqual( JSON.stringify([{"uuid":"d123654"}]));
    }, function(error){
      debugger;
      console.log("error");
    });
  });

  after(function () { 
    server.restore(); 
  });

/*
   before(function () {
     server = sinon.fakeServer.create();
     server.respondWith("POST", "http://localhost:3000/saveAssetsDataPost", [
    200, 
    {"Content-Type": "application/json"}, 
    JSON.stringify([{"success":true}])
    ]);
  });

  it('save PMD should work fine when mocking service', () => {
    console.log("server respond called");
   let values={ 
    productName: 'abc',
    author: 'def',
    isbnOptional: 'hssh',
    publisher: 'pears',
    disciplines: 'history'

  };
    let successdata = productMetaDataApi.save_PMD_Data(values);
    server.respond();
    successdata.then(function(data){ 
      debugger;
      console.log("data");
      expect(data.text).toEqual(true);

    }, function(error){
      debugger;
      console.log("error");
    });
  });

  after(function () { 
    server.restore(); 
  });

*/


   before(function () {
     server = sinon.fakeServer.create();
     server.respondWith("GET", "http://localhost:3000/assetsDataSelectBox'", [
    404, 
    {"Content-Type": "application/json"}, 
    JSON.stringify({"uuid":"d123654"})
    ]);
  });

  it('print the error when mocking service', () => {
    console.log("server respond called");
    let successdata = productMetaDataApi.get_PMD_Data();
    server.respond();
    successdata.then(function(data){ 
      debugger;
      console.log("data");
    }, function(error){
      debugger;
      console.log("error",error);
      expect(error.message).toEqual("cannot GET http://localhost:3000/assetsDataSelectBox (404)");
    });
  });

  after(function () { 
    server.restore(); 
  });

});
